import React from 'react'
import ReactDOM from 'react-dom'
import './index.less'
import App from './App'
import { Theme, createTheme, ThemeProvider } from '@fluentui/react';
import { currentTheme, themeToCssVars } from "./components/Theme";

const cssTheme = themeToCssVars(currentTheme);
const style = document.createElement("style");

style.innerHTML = ":root {" + cssTheme + "}"
document.head.appendChild(style);

ReactDOM.render(
	<ThemeProvider applyTo="body" theme={currentTheme}>
		<React.StrictMode>
			<App />
		</React.StrictMode>
	</ThemeProvider>,
	document.getElementById('root')
)
