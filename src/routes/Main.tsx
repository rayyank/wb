import UserMessagesContainer from "../components/userMessagesContainer/UserMessagesContainer";

function Main() {
	return (
		<div>
			<UserMessagesContainer />
		</div>
	);
}

export default Main;